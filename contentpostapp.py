#!/usr/bin/python3

import webapp


form = """
    <br/><br/>
    <form action="" method = "POST"> 
    <input type="text" name="name" value="">
    <input type="submit" value="Enviar">
    </form>
"""

mensaje_ok = """
<!DOCTYPE html>
<html>
  <body>
    {content}
    {form}
  </body>
</html>
"""

mensaje_not_found = """
<!DOCTYPE html>
<html>
  <body>
    Recurso {resource} no disponible.
    {form}
  </body>
</html>
"""


class contentPostApp (webapp.webApp):
    """Simple web application for managing content."""

    content_dicc = {'/': 'Root page', '/page': 'A page', '/hola': 'Hola',
                    '/adios': 'Adios', '/prueba': 'Esto es una prueba'}

    def parse(self, request):
        """Return the resource name (including /), method and body"""

        method = request.split(' ', 2)[0]  # Trocea con espacio 2 veces y se queda el metodo
        resource = request.split(' ', 2)[1]  # Trocea con espacio 2 veces y se queda el recurso (incluye /)
        body = request.split('\r\n\r\n', 1)[1]  # Trocea /r/n/r/n 1 vez se queda con el cuerpo
        print(request.split(' '))

        return method, resource, body

    def process(self, info):
        """Process the relevant elements of the request."""

        method, resource, body = info
        print('\r\nMétodo: ' + method, '\r\nRecurso: ' + resource, '\r\nBody: ' + body)

        if method == "POST":  # Método POST
            self.content_dicc[resource] = body.split("=")[1]  # Guarda en el diccionario el contenido del
            # cuerpo (valor del formulario) asociado al nuevo recurso o modifica el ya existente

        # Método GET contemplado aquí (en este caso si no es POST solo puede ser GET y no cambia el resto)
        if resource in self.content_dicc.keys():
            httpCode = "200 OK"
            # GET y POST muestran ambos el formulario y el contenido (en el caso de POST tras mandar el formulario)
            content = self.content_dicc[resource]
            # Se muestra el contenido (el recurso se encuentra en el diccionario de contenidos)
            htmlBody = mensaje_ok.format(content=content, form=form)
        else:  # Si no se encuentra el recurso se devuelve un error.
            httpCode = "404 Not Found"
            htmlBody = mensaje_not_found.format(resource=resource, form=form)
        return (httpCode, htmlBody)


if __name__ == "__main__":
    miWebApp = contentPostApp("localhost", 1234)
